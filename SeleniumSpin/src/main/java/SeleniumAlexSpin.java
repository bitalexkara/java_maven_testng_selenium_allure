import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SeleniumAlexSpin {
    @Test
    public void myTest() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "/Users/Oleksandr_Kara/Desktop/drivers2/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.get("https://formy-project.herokuapp.com/form");
        driver.findElement(By.id("first-name")).sendKeys("Alex");
        driver.findElement(By.id("last-name")).sendKeys("Kara");
        driver.findElement(By.xpath("/html/body/div/form/div/div[8]/a")).click();
        Thread.sleep(1000);

        Assert.assertEquals(driver.findElement(By.className("alert-success")).getText(), "The form was successfully submitted!");
        driver.close();
    }
}
